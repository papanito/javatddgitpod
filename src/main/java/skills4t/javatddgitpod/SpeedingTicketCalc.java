package com.skills4t.javatddgitpod;

public class SpeedingTicketCalc {
   
    public boolean isTicket(int velocity, int speedlimit) {
        return velocity > speedlimit;
    }

    public int getPenalty(int velocity, int speedlimit, String roadType) {
        int speedDiff = velocity - speedlimit;
        switch (roadType) {
            case "tempo30":
               return getPenaltyZone30(speedDiff);
            default:
                return 0;
        }
    }

    public int getPenaltyZone30(int speedDiff) {
        if ((1 <= speedDiff) && (speedDiff <= 5)) {
            return 40;
        }
        if ((6 <= speedDiff) && (speedDiff <= 10)) {
            return 120;
        }
        if ((11 <= speedDiff) && (speedDiff <= 15)) {
            return 250;
        }
        if ((16 <= speedDiff) && (speedDiff <= 17)) {
            return 400;
        }
        if ((18 <= speedDiff) && (speedDiff <= 19)) {
            return 600;
        }
        return 0; //TBD: what if < 19km/h
    }
}

