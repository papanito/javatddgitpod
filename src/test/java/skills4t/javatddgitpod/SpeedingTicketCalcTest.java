package com.skills4t.javatddgitpod;

import org.junit.Test;

import static org.junit.Assert.*;

public class SpeedingTicketCalcTest {

    @Test
    public void isTicketTest() {
        SpeedingTicketCalc calc = new SpeedingTicketCalc();
        assertTrue(calc.isTicket(60,50));
    }

    @Test
    public void getPenaltyTest() {
        SpeedingTicketCalc calc = new SpeedingTicketCalc();
        assertEquals(40, calc.getPenalty(35,30,"tempo30")); //1-5 km/h
        assertEquals(120, calc.getPenalty(36,30,"tempo30")); //6-10 km/h
        assertEquals(250, calc.getPenalty(45,30,"tempo30")); //11-15 km/h
        assertEquals(400, calc.getPenalty(47,30,"tempo30")); //16-17 km/h
        assertEquals(600, calc.getPenalty(49,30,"tempo30")); //18-19 km/h
    }
}
